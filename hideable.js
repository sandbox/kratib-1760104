(function ($) {

  Drupal.behaviors.hideable = {
    attach: function(context, settings) {
      $('.hideable', context).once('hideable', function() {
        var data = $(this).data('hideable'), hide = Drupal.t('Hide'), show = Drupal.t('Show'), style = '';
        if (data) {
          if (data.title) {
            hide = Drupal.t('Hide @title', {'@title': data.title});
            show = Drupal.t('Show @title', {'@title': data.title});
          }
          if (data.style) {
            style = data.style;
          }
        }
        $(this).wrap('<div class="hideable-wrapper"></div>');
        var $hide = $('<span class="hideable-hide hideable-icon"></span>').attr('style', style).show().insertBefore(this).attr('title', hide);
        var $show = $('<span class="hideable-show hideable-icon"></span>').attr('style', style).hide().insertBefore(this).attr('title', show);
      });
      $('.hideable-hide', context).click(function(event) {
        $(this).hide();
        $(this).siblings('.hideable').hide();
        $(this).siblings('.hideable-show').show();
      });
      $('.hideable-show', context).click(function(event) {
        $(this).hide();
        $(this).siblings('.hideable').show();
        $(this).siblings('.hideable-hide').show();
      });
    }
  };

})(jQuery);
